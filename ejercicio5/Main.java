public class Main {
    public static void main(String args[]) {
        // # Ejercicio 5

        Pelicula pelicula = new Pelicula("Jhon Wick 4", "Accion", 169, "PM18", "Chad Stahelski");
        Pelicula pelicula2 = new Pelicula("Mario Bross", "Aventura, Animacion", 92, "ATP", " Aaron Horvath");

        pelicula.mostrarDatos();
        pelicula2.mostrarDatos();
    }
}