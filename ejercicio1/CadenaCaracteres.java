
// package ejercicio1;

// Debe coincidir con el nombre del archivo
public class CadenaCaracteres {

    // Privamos el atributo cadena para que no sea alterado

    private String cadena;

    // El constructor a la hora de llamar a la instanciación
    public CadenaCaracteres(String cadena) {
        this.cadena = cadena;
    }

    // MÉTODOS

    // 1) obtenemos la mitad de la cadena
    public String obtenerPrimeraMitad() {
        return cadena.substring(0, cadena.length() / 2);
    }

    // 2) convertimos a mayúscula la cadena entera
    public String obtenerEnMayuscula() {
        return cadena.toUpperCase();
    }

    // 3) 'charAt' es un método que nos permite pasarle un entero
    // y obtener el caracter en esa posición de la cadena
    public char obtenerUltimoCaracter() {
        return cadena.charAt(cadena.length() - 1);
    }

    // 4) obtener string invertido
    public String obtenerEnFormaInversa() {
        // StringBuilder es una clase, similar a String
        // pero 'String' no puede mutar sus elementos
        // pero StringBuilder puede hacer uso de los
        // siguientes modificadores:
        // append(), insert(), delete(), replace(), reverse()
        // con delete() pasamos los índices desde dónde hasta dónde queremos eliminar
        // con insert() pasamos el índice y el valor en string de lo que usaremos para
        // reemplazar
        StringBuilder sb = new StringBuilder(cadena);
        return sb.reverse().toString();
    }

    // 5)separar por guiones
    public String obtenerSeparadoPorGuion() {
        // uso de los métodos join()
        // join() recibe un separador, y un array
        // split() recibe un separador desde el cual partirá
        // los elementos dentro del string
        return String.join("-", cadena.split(""));
    }

    // 6) cantidad de vocales
    // Usamos toLowerCase para convertir las vocales mayusculas a minusculas
    // para evitar otra serie de comparacion con cada vocal en mayuscula
    public int obtenerCantidadDeVocales() {
        int cantidad = 0;
        for (int i = 0; i < cadena.length(); i++) {
            char c = Character.toLowerCase(cadena.charAt(i));
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                cantidad++;
            }
        }
        return cantidad;
    }

}