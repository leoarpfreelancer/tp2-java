// package ejercicio6;

//BIG DECIMAL NECESITA UN STRING: //NOTA: PARECE QUE PUEDE USAR un INT (Pero trabajaremos con string. Leer documentación)
//Para crear un objeto BigDecimal, se debe pasar un valor de tipo String
//que representa el número decimal, en lugar de pasar directamente el 
//valor numérico como se haría con los tipos de datos de punto flotante.

//import java.math.BigDecimal;

// Crear un objeto BigDecimal con un valor de 0.1
// BigDecimal decimal = new BigDecimal("0.1");

// Realizar operaciones con BigDecimal
// BigDecimal resultado = decimal.multiply(new BigDecimal("2"));

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Producto {

    // Atributos
    // Cod y Nombre
    private String codProducto;
    private String nombre;
    // Precio
    private BigDecimal precioCosto;
    private String porcentajeGanancia;
    private BigDecimal porcentajeIva;
    private BigDecimal precioVenta;

    // MAYOR
    // un estático es un valor único para todos los objetos de la clase
    // en este caso particular deseamos guardar el objeto que a la hora de
    // su creación sea el de mayor valor
    private static Producto mayor;

    // Constructor;
    // Trabajaremos como String la mayoría de los elementos porque es una condición
    // para trabajar con BigDecimal
    public Producto(String codProducto, String nombre, String precioCosto, String porcentajeGanancia,
            String porcentajeIva, String precioVenta) {
        this.codProducto = codProducto;
        this.nombre = nombre;
        // el resto lo operaremos con getters y setters
        setPrecioCosto(precioCosto);
        setPorcentajeGanancia(porcentajeGanancia);
        setPorcentajeIva(porcentajeIva);
        // Obtenido de la suma del precio de costo + el porcentaje de ganancia
        setPrecioVenta("");

        // Guardamos el objeto con mayor valor de precio
        if (mayor == null) {
            mayor = this;
        } else {
            // CompareTo es el método nativo de BigDecimal
            // para comparar dos BigDecimals
            // si el primero es mayor al segundo será mayor a 0
            // si el segundo es mayor será menor a 0
            // si son iguales será 0
            if (this.getPrecioVenta().compareTo(mayor.getPrecioVenta()) > 0) {
                mayor = this;
            }
        }

    }

    // GETTERS y SETTERS

    // Obtener el mayor
    public Producto getMayor() {
        return mayor;
    }

    // codProducto
    public String getCodProducto() {
        return this.codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    // nombre
    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    // precioCosto
    public BigDecimal getPrecioCosto() {
        return this.precioCosto;
    }

    public void setPrecioCosto(String precioCosto) {
        this.precioCosto = new BigDecimal(precioCosto);
    }

    // porcentajeGanancia
    // GET
    public String getPorcentajeGanancia() {
        return this.porcentajeGanancia;
    }

    // SET
    public void setPorcentajeGanancia(String porcentajeGanancia) {
        if (porcentajeGanancia != "" || porcentajeGanancia != null) {

            this.porcentajeGanancia = porcentajeGanancia;

        }

    }

    // porcentajeIva
    public BigDecimal getPorcentajeIva() {
        return this.porcentajeIva;
    }

    public void setPorcentajeIva(String porcentajeIva) {
        if (porcentajeIva == "") {
            this.porcentajeIva = new BigDecimal("21");
            return;
        }

        this.porcentajeIva = new BigDecimal(porcentajeIva);
    }

    // precioVenta
    public BigDecimal getPrecioVenta() {
        return this.precioVenta;
    }

    public void setPrecioVenta(String precioVenta) {

        if (precioVenta == "" || precioVenta == null) {

            // En esta parte vamos a DIVIDIR POR 100 el BIG DECIMAL
            // Declaramos las variables
            BigDecimal porcentajeGananciaDividir = new BigDecimal(porcentajeGanancia);
            BigDecimal divisor = new BigDecimal("100");

            // Dividimos y Asignamos
            BigDecimal resultado = porcentajeGananciaDividir.divide(divisor, RoundingMode.DOWN);

            // El uso del método 'add' no muta el valor original
            this.precioVenta = this.precioCosto.add(this.precioCosto.multiply(resultado));
        } else {

            this.precioVenta = new BigDecimal(precioVenta);
        }
    }

}